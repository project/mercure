# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3] - 2023-12-28
### Changed
- Change dependency to [symfony/mercure-bundle](https://github.com/symfony/mercure-bundle)
- Mark Drupal 10 and Mercure 0.6 compatible

## [1.0.2] - 2022-06-02
### Changed
- Adhere to Drupal Coding Standards

### Removed
- Remove sync with Github
- Remove license because it's added automatically

## [1.0.1] - 2021-07-02
### Changed
- Use CompilerPass to register services
- Add sync with Github

## [1.0.0] - 2021-07-02
Initial release
